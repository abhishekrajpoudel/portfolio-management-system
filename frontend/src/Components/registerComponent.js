import React from "react";

const commonFields = {
  name: "",
  email: "",
  password: "",
  re_password: "",
  gender: "",
};

export class Register extends React.Component {
  constructor() {
    super();
    this.state = {
      data: { ...commonFields },
      error: { ...commonFields },
      isSubmitting: false,
    };
  }
  handleChange = (event) => {
    const { name, value, check } = event.target;

    this.setState(
      (previousState) => {
        return {
          data: { ...previousState.data, [name]: value },
        };
      },
      () => {
        this.validateForm(name);
      }
    );
  };
  validateForm = (fieldName) => {
    let errorMessage = null;
    const { data } = this.state;
    switch (fieldName) {
      case "name":
        errorMessage = data[fieldName] != null ? "" : "Name is required";
        break;

      case "email":
        errorMessage =
          data[fieldName] != null
            ? data[fieldName].includes("@")
              ? ""
              : "Invalid email address"
            : "Name is required";
        break;

      case "password":
        errorMessage =
          data[fieldName] != null
            ? data[fieldName].length >= 8
              ? ""
              : "Password not long enough"
            : "Password is required";
        break;

      case "gender":
        errorMessage = data[fieldName] != null ? "" : "Gender is required";
        break;
    }

    this.setState(
      (previousError) => ({
        error: {
          ...previousError.error,
          [fieldName]: errorMessage,
        },
      }),
      () => {
        const { error } = this.state;
        let error_count = 0;
        for (let key in commonFields) {
          if (error[key]) {
            error_count++;
          }
        }
        this.setState({
          isSubmittingForm: error_count !== 0 ? true : false,
        });
      }
    );
  };
  handleSubmit = (event) => {
    event.preventDefault();
    for (let key in commonFields) {
      this.validateForm(key);
    }
  };

  render() {
    const { error } = this.state;
    return (
      <div className="container">
        <div className="row">
          <div className="col-12">
            <h4 className="text-center">Register Page</h4>
            <hr />
            <div className="row mb-3">
              <form action="" method="post">
                <div className="mb-3">
                  <label className="form-label text-center">Name</label>
                  <input
                    onChange={this.handleChange}
                    className="form-control"
                    name="name"
                    type="text"
                    className="form-control"
                    placeholder="Enter your name"
                    required
                  />
                  <span className="text-danger">{error.name}</span>
                </div>
                <div className="mb-3">
                  <label className="form-label text-center">Email</label>
                  <input
                    onChange={this.handleChange}
                    className="form-control"
                    name="email"
                    type="email"
                    className="form-control"
                    placeholder="Enter your email"
                    required
                  />
                  <span className="text-danger">{error.email}</span>
                </div>
                <div className="mb-3">
                  <label className="form-label text-center">Password</label>
                  <input
                    onChange={this.handleChange}
                    className="form-control"
                    name="password"
                    type="password"
                    className="form-control"
                    placeholder="Enter your password"
                    required
                  />
                  <span className="text-danger">{error.password}</span>
                </div>
                <div className="mb-3">
                  <label className="form-label text-center">Re-Password</label>
                  <input
                    onChange={this.handleChange}
                    className="form-control"
                    name="re_password"
                    type="password"
                    className="form-control"
                    placeholder="Enter password again"
                    required
                  />
                  <span className="text-danger">{error.re_password}</span>
                </div>
                <div className="row mb-3">
                  <label className="col-sm-1">Gender</label>
                  <div className="col-sm-1">
                    <label htmlFor="male">
                      <input
                        onChange={this.handleChange}
                        name="gender"
                        type="radio"
                        value={"Male"}
                        id="male"
                        className="ml-3"
                      />{" "}
                      Male
                    </label>
                  </div>

                  <div className="col-sm-1">
                    <label htmlFor="female">
                      <input
                        onChange={this.handleChange}
                        name="gender"
                        type="radio"
                        value={"Female"}
                        id="female"
                        className="ml-3"
                      />{" "}
                      Female
                    </label>
                  </div>

                  <div className="col-sm-1">
                    <label htmlFor="Other">
                      <input
                        onChange={this.handleChange}
                        name="gender"
                        type="radio"
                        value={"Other"}
                        id="Other"
                        className="ml-3"
                      />{" "}
                      Other
                    </label>
                  </div>
                  <span className="text-denger">{error.gender}</span>
                </div>
                <button
                  onClick={this.handleSubmit}
                  type="submit"
                  className="btn btn-success"
                  disabled={this.state.isSubmittingForm}
                >
                  Submit
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

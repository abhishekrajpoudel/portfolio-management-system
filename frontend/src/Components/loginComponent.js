import React from "react";

export class Login extends React.Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      isSubmitting: false,
    };
  }

  handleChange = (event) => {
    const { name, value } = event.target;
    this.setState({
      [name]: value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    this.setState({
      isSubmitting: true,
    });
  };
  render() {
    return (
      <div className="container">
        <h4 className="text-center">Login</h4>
        <hr />
        <form action="" method="post" onSubmit={this.handleSubmit}>
          <div className="mb-3">
            <label htmlFor="" className="form-label text-centerd">
              Email Address
            </label>
            <input
              name="email"
              type="email"
              className="form-control"
              placeholder="Enter Your Email"
              onChange={this.handleChange}
            />
          </div>
          <div className="mb-3">
            <label htmlFor="" className="form-label text-centerd">
              Password
            </label>
            <input
              name="password"
              type="password"
              className="form-control"
              placeholder="Enter Your Password"
              onChange={this.handleChange}
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </form>
      </div>
    );
  }
}

const router = require("express").Router();
const UserModel = require("../models/user.model");
const ContactModel = require("../models/contact.model");
const {
  getAllData,
  addData,
  getDataById,
  updateDataById,
  deleteDataById,
} = require("../controller/base.controller");

function baseRouter(name, Model, outputName) {
  router
    .route(`${name}/`)
    // .route("/")
    .get((req, res, next) => {
      getAllData(res, Model, outputName + "s");
    })
    .post((req, res, next) => {
      let data = new Model(req.body);
      addData(res, data, outputName);
    });

  router
    .route(`${name}/:id`)
    // .route("/:id")
    .get((req, res, next) => {
      getDataById(req, res, Model, outputName);
    })
    .put((req, res, next) => {
      let data = req.body;
      updateDataById(req, res, Model, data, outputName);
    })
    .delete((req, res, next) => {
      deleteDataById(req, res, Model, outputName);
    });
  return router;
}

module.exports = baseRouter;
// module.exports = router;

const router = require("express").Router();

//router
const userRoute = require("./userRoute");
const contactRoute = require("./contactRoute");
const projectsRoute = require("./projectsRoute");
const portfolioRoute = require("./portfolioRoute");
const adminRoute = require("./adminRoute");
const authRoute = require("./authRoute");

//middleware
const isLoggedIn = require("../middleware/isLoggedIn");
const isAdmin = require("../middleware/isAdmin");

//authentication route
router.use("/auth", authRoute);

//routes
router.use("/admin", [isLoggedIn, isAdmin], adminRoute);

router.use("/contact", contactRoute);
router.use("/project", projectsRoute);
router.use("/portfolio", portfolioRoute);
router.use("/user", userRoute);

module.exports = router;

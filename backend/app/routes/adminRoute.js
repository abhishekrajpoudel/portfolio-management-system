const router = require("express").Router();
const { adminDashboard } = require("../controller/base.controller");

router.route("/").get(adminDashboard);

module.exports = router;

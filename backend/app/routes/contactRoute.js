const router = require("express").Router();
const ContactModel = require("../models/contact.model");
const {
  getAllData,
  addData,
  getDataById,
  updateDataById,
  deleteDataById,
} = require("../controller/base.controller");

const contactOutput = "Contact";

router
  .route("/")
  .get((req, res, next) => {
    getAllData(res, ContactModel, contactOutput + "s");
  })
  .post((req, res, next) => {
    let user = new ContactModel(req.body);
    addData(res, user, contactOutput);
  });

router
  .route("/:id")
  .get((req, res, next) => {
    getDataById(req, res, ContactModel, contactOutput);
  })
  .put((req, res, next) => {
    let data = req.body;
    updateDataById(req, res, ContactModel, data, contactOutput);
  })
  .delete((req, res, next) => {
    deleteDataById(req, res, ContactModel, contactOutput);
  });

module.exports = router;

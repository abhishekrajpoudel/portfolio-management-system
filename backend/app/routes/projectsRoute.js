const router = require("express").Router();
const ProjectModel = require("../models/projects.model");
const {
  getAllData,
  addData,
  getDataById,
  updateDataById,
  deleteDataById,
  uploadImage,
} = require("../controller/base.controller");
const uploader = require("../middleware/uploader");

const projectOutput = "project";
// for image uploading
const imageUploadList = ["thumnail", "referance_image"];
const multipleUploads = uploader.fields([
  { name: imageUploadList[0] },
  { name: imageUploadList[2], maxCount: 5 },
]);

router
  .route("/")
  .get((req, res, next) => {
    getAllData(res, ProjectModel, `${projectOutput}s`);
  })
  .post(multipleUploads, (req, res, next) => {
    let data = new ProjectModel(req.body);
    data = uploadImage(req, data, imageUploadList);
    addData(res, data, projectOutput);
  });

router
  .route("/:id")
  .get((req, res, next) => {
    getDataById(req, res, ProjectModel, projectOutput);
  })
  .put(multipleUploads, (req, res, next) => {
    let data = req.body;
    data = uploadImage(req, data, imageUploadList);
    updateDataById(req, res, ProjectModel, data, projectOutput);
  })
  .delete((req, res, next) => {
    deleteDataById(req, res, ProjectModel, projectOutput);
  });

module.exports = router;

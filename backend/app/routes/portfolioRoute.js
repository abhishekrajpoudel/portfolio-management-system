const router = require("express").Router();
const PortfolioModel = require("../models/portfolio.model");
const {
  getAllData,
  addData,
  getDataById,
  updateDataById,
  deleteDataById,
  uploadImage,
} = require("../controller/base.controller");

const uploader = require("../middleware/uploader");

const portfolioOutput = "Portfolio";
const imageUploadList = ["portfolio_image", "banner_image"];
const multiple_uploads = uploader.fields([
  { name: imageUploadList[0] },
  { name: imageUploadList[1], maxCount: 5 },
]);

router
  .route("/")
  .get((req, res, next) => {
    getAllData(res, PortfolioModel, `${portfolioOutput}s`);
  })
  .post(multiple_uploads, (req, res, next) => {
    let data = new PortfolioModel(req.body);
    data = uploadImage(req, data, imageUploadList);
    addData(res, data, portfolioOutput);
  });

router
  .route("/:id")
  .get((req, res, next) => {
    getDataById(req, res, PortfolioModel, portfolioOutput);
  })
  .put(multiple_uploads, (req, res, next) => {
    let data = req.body;
    data = uploadImage(req, data, imageUploadList);
    updateDataById(req, res, PortfolioModel, data, portfolioOutput);
  })
  .delete((req, res, next) => {
    deleteDataById(req, res, PortfolioModel, portfolioOutput);
  });

module.exports = router;

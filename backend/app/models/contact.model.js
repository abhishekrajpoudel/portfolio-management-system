const mongoose = require("mongoose");

const ContactSchema = new mongoose.Schema(
  {
    portfolio_id: {
      type: mongoose.Types.ObjectId,
      ref: "Portfolio",
    },

    full_name: String,

    gender: {
      type: String,
      enum: ["male", "female", "Other"],
      default: "male",
    },

    date_of_birth: Date,

    location: String,

    business_email: String,

    phone_number: [Number],

    website: [
      {
        site_name: String,
        link: String,
      },
    ],
  },
  { timestamps: true }
);

const Contact = mongoose.model("Contact", ContactSchema);
module.exports = Contact;

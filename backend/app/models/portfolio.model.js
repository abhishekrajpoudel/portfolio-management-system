const mongoose = require("mongoose");

const PortfolioSchema = new mongoose.Schema(
  {
    portfolio_image: {
      type: String,
      required: true,
    },

    banner_image: [
      {
        type: String,
        required: true,
      },
    ],

    hero_title: {
      type: String,
      required: true,
    },

    hero_description: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

const Portfolio = mongoose.model("Portfolio", PortfolioSchema);

module.exports = Portfolio;

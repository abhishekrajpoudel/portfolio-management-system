const mongoose = require("mongoose");
const projectsSchema = new mongoose.Schema({
  portfolio_id: {
    type: mongoose.Types.ObjectId,
    ref: "Portfolio",
  },

  type: String,

  tools_used: [String],

  thumnail: String,

  referance_image: [String],

  title: String,

  discription: String,

  demo_link: String,

  start_date: Date,

  end_date: Date,
});

const Projects = mongoose.model("Projects", projectsSchema);
module.exports = Projects;

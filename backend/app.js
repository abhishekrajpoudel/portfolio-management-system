var createError = require("http-errors");
var express = require("express");

var apiRouter = require("./app/routes/api");
var app = express();

require("./db_init");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/assets", express.static(process.cwd() + "/public"));

//all routes
app.use("/api", apiRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  console.error(err.message);
  res.json({
    data: null,
    status: 500,
    msg: err,
  });
});

module.exports = app;
